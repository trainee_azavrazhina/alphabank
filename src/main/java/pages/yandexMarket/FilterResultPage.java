package pages.yandexMarket;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;

import java.util.List;


public class FilterResultPage extends BasePage {

    @FindBy(xpath = ".//div[contains(@class, 'filter-sorter')]")
    WebElement initElement;

    @FindBy(xpath = ".//div[@data-id]//div[contains(@class,'title')]//a")
    private List<WebElement> elementsNumberCells;


    @FindBy(xpath = ".//input[@id='header-search']")
    public WebElement searchField;

    @FindBy(xpath = ".//span[@class='search2__button']")
    public WebElement searchButton;

    static String firstResultName;

    public FilterResultPage() {
        super();
        waitElementVisibility(initElement);
    }

    public void checkElementsNumber(int expectedNumber) {
        int productNumber = elementsNumberCells.size();
        Assert.assertEquals("Количество отображаемых товаров не равно ожидаемому количеству", expectedNumber, productNumber);
    }

    public void searchingModel() {
        // Запомнить первый элемент в списке
        firstResultName = elementsNumberCells.get(0).getText();
        elementsNumberCells.get(0).click();
    }

}
