package pages.yandexMarket;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;

public class FiltersPage extends BasePage {

    @FindBy(xpath = ".//span[@sign-title='от']//input")
    public WebElement minPriceField;

    @FindBy(xpath = ".//span[@sign-title='до']//input")
    public WebElement maxPriceField;

    @FindBy(xpath = ".//div[contains(@data-filter-id,'7893318')]")
    public WebElement manufacturer;

    @FindBy(xpath = ".//span[text()='Показать подходящие']/..")
    public WebElement accept;

    @FindBy(xpath = ".//h1[@title='Все фильтры']")
    WebElement initElement;

    public FiltersPage() {
        super();
        waitElementVisibility(initElement);
    }

    public void setMinPrice(String initialPrice) {
        minPriceField.click();
        minPriceField.clear();
        minPriceField.sendKeys(initialPrice);
    }

    public void setMaxPrice(String initialPrice) {
        if (!initialPrice.equals("Не задано")) {
            maxPriceField.click();
            maxPriceField.clear();
            maxPriceField.sendKeys(initialPrice);
        }
    }

    public void setManufacturer(String name) {
        manufacturer.findElement(By.xpath(".//label[contains(text(), '" + name + "')]")).click();
    }

    public void pushAccept() {
        accept.click();
    }
}
