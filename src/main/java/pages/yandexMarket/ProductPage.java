package pages.yandexMarket;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;

public class ProductPage extends BasePage {

    @FindBy(xpath = ".//a[contains(text(), 'Перейти ко всем фильтрам')]")
    public WebElement goToAllFilters;

    @FindBy(xpath = ".//div[contains(@class, 'filter-sorter')]")
    WebElement initElement;

    public ProductPage() {
        super();
        waitElementVisibility(initElement);
    }

    public void selectAdvancedPage() {
        goToAllFilters.click();
    }

}
