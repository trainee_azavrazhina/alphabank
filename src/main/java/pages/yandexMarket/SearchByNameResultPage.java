package pages.yandexMarket;

import org.junit.Assert;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;

public class SearchByNameResultPage extends BasePage {

    @FindBy(xpath = ".//div[@class='n-product-title__text-container']//h1")
    public WebElement actualTitle;

    @FindBy(xpath = ".//a[contains(@class, 'reviews link n-smart-link')]")
    WebElement initElement;

    public SearchByNameResultPage() {
        super();
        waitElementVisibility(initElement);
    }

    public void checkTitles() {
        try {
            Assert.assertEquals("Найден не тот объект", FilterResultPage.firstResultName, actualTitle.getText());
        } catch (NoSuchElementException e) {
            System.out.println("Названия моделей полностью не совпадают");
        }

    }
}
