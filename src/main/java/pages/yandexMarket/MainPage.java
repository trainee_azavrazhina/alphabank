package pages.yandexMarket;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;

public class MainPage extends BasePage {
    /**
     * Главная страница Яндекса
     */
    @FindBy(xpath = ".//div[@class='home-arrow__tabs']")
    WebElement menuItem;

    public MainPage() {
        super();
        waitTitle("Яндекс");
    }

    public void selectMenuItem(String itemName) {
        menuItem.findElement(By.xpath(".//a[contains(text(), '" + itemName + "')]")).click();
    }
}
