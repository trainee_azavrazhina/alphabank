package pages.yandexMarket;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;

public class MarketPage extends BasePage {
    /**
     * Страница Яндекс.Маркет
     */

    @FindBy(xpath = ".//ul[contains(@class, 'topmenu__list')] | .//div[@class='n-navigation-horizontal__inner']")
    WebElement menuItem;

    @FindBy(xpath = ".//a[contains(@class, 'logo')]/span[text()='Яндекс']")
    WebElement initElement;

    public MarketPage() {
        super();
        waitElementVisibility(initElement);
    }

    public void selectMenuItem(String itemName) {
        menuItem.findElement(By.xpath(".//a[contains(text(), '" + itemName + "')]")).click();
    }
}
