package pages.yandexMarket;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;

public class ElectronicsPage extends BasePage {

    @FindBy(xpath = ".//div[contains(@class, 'catalog-menu i-bem')]")
    WebElement menuItem;

    @FindBy(xpath = ".//h1[@title='Электроника']")
    WebElement initElement;

    @FindBy(xpath = "(.//a[contains(@class, 'type_more')])[1]")
    WebElement showMore;

    public ElectronicsPage() {
        super();
        waitElementVisibility(initElement);
    }

    public void chooseCategory(String product) {
        showMore.click();
        waitElementVisibility(menuItem.findElement(By.xpath(".//descendant::a[contains(text(), '" + product + "')]")));
        menuItem.findElement(By.xpath(".//descendant::a[contains(text(), '" + product + "')]")).click();
    }
}
