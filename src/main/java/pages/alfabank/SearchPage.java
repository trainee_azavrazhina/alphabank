package pages.alfabank;


import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;
import steps.BaseSteps;

public class SearchPage extends BasePage {
    public static final String searchSys = BaseSteps.getDriver().getTitle();

    @FindBy(xpath = ".//input[@title='Search']")
    WebElement searchField;

    public SearchPage() {
        super();
        waitTitle("Google");
    }

    public void fillSearchField(String words) {
        searchField.click();
        searchField.clear();
        searchField.sendKeys(words);
        searchField.sendKeys(Keys.RETURN);
    }
}
