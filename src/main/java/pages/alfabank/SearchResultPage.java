package pages.alfabank;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;
import steps.BaseSteps;

import java.util.ArrayList;

public class SearchResultPage extends BasePage {

    @FindBy(xpath = ".//h3[@class='r']/a[contains(text(), 'Альфа-Банк - кредитные')]")
    WebElement alphaBankLink;

    @FindBy(xpath = ".//div[@id='resultStats']")
    WebElement initElement;

    public SearchResultPage() {
        super();
        waitElementVisibility(initElement);
    }

    public void openFirstResult() {
        alphaBankLink.click();
        // Переключение на 2ую вкладку
        ArrayList<String> tabs = new ArrayList<>(BaseSteps.getDriver().getWindowHandles());
        BaseSteps.getDriver().switchTo().window(tabs.get(1));
    }
}
