package pages.alfabank;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;
import steps.BaseSteps;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AboutJobPage extends BasePage {

    @FindBy(xpath = ".//h1[text()='О работе в банке']")
    WebElement initElement;

    @FindBy(xpath = ".//div[@class='message']")
    WebElement message;

    @FindBy(xpath = ".//div[@class='info']")
    WebElement info;

    public AboutJobPage() {
        super();
        waitElementVisibility(initElement);
    }

    public void printInfoToFile() {
        // Подготовка имени файла
        String currentDateTime = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        Capabilities cap = ((RemoteWebDriver) BaseSteps.getDriver()).getCapabilities();
        String browserName = cap.getBrowserName().toLowerCase();
        String fileName = currentDateTime + " " + browserName + " " + SearchPage.searchSys + ".txt";

        new Actions(BaseSteps.getDriver()).moveToElement(message).perform();
        try (FileWriter writer = new FileWriter(fileName, false)) {
            writer.write(message.getText() + "\r\n" + "\r\n");
            List<WebElement> p = new ArrayList<>(info.findElements(By.xpath(".//p")));
            writer.write(p.get(0).getText() + "\r\n" + "\r\n");
            writer.write(p.get(1).getText() + "\r\n" + "\r\n");
            List<WebElement> ul1 = new ArrayList<>(info.findElements(By.xpath(".//ul[1]/li")));
            for (WebElement el : ul1) {
                writer.write(el.getText() + "\r\n");
            }
            writer.write(p.get(2).getText() + "\r\n" + "\r\n");
            List<WebElement> ul2 = new ArrayList<>(info.findElements(By.xpath(".//ul[2]/li")));
            for (WebElement el : ul2) {
                writer.write(el.getText() + "\r\n");
            }
            writer.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
