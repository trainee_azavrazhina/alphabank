package pages.alfabank;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;

public class JobPage extends BasePage {

    @FindBy(xpath = ".//img[@alt='Альфа-банк']")
    WebElement initElement;

    @FindBy(xpath = ".//nav[@class='nav']")
    WebElement navigation;

    public JobPage() {
        super();
        waitElementVisibility(initElement);
    }

    public void chooseSection(String section) {
        navigation.findElement(By.xpath(".//descendant::span[contains(text(), '" + section + "')]/..")).click();
    }
}
