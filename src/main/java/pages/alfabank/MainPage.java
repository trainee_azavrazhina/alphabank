package pages.alfabank;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;
import steps.BaseSteps;

public class MainPage extends BasePage {

    @FindBy(xpath = ".//span[contains(text(), 'Частным лицам')]")
    WebElement initElement;

    @FindBy(xpath = ".//div[contains(@class, 'footer')]")
    WebElement menuItem;

    public MainPage() {
        super();
        waitElementVisibility(initElement);
    }

    public void chooseOptionFromBottomMenu(String option) {
        new Actions(BaseSteps.getDriver()).moveToElement(menuItem).perform();
        menuItem.findElement(By.xpath(".//descendant::a[contains(text(), '" + option + "')]")).click();
    }
}
