package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import steps.BaseSteps;

public class BasePage {
    //Значение для WebDriverWait по умолчанию из application.properties
    public static final int defaultWait = Integer.parseInt(BaseSteps.properties.getProperty("default_wait"));

    public BasePage() {
        PageFactory.initElements(BaseSteps.getDriver(), this);
    }

    public void waitElementVisibility(WebElement el) {
        // Ожидание видимости элемента со значением по умолчанию
        new WebDriverWait(BaseSteps.getDriver(), defaultWait).until(ExpectedConditions.visibilityOf(el));
    }

    public void waitElementVisibility(WebElement el, int sec) {
        new WebDriverWait(BaseSteps.getDriver(), sec).until(ExpectedConditions.visibilityOf(el));
    }

    public static void waitTitle(String title) {
        new WebDriverWait(BaseSteps.getDriver(), defaultWait).
                until(ExpectedConditions.titleIs(title));
    }

    public void getPage(String url) {
        BaseSteps.getDriver().get(url);
        BaseSteps.getDriver().manage().window().maximize();
    }

}
