package steps.yandexMarket;

import pages.yandexMarket.FilterResultPage;
import ru.yandex.qatools.allure.annotations.Step;

public class FilterResultSteps {

    @Step("сохранено значение первого товара")
    public void searchingModel() {
        new FilterResultPage().searchingModel();
    }
}
