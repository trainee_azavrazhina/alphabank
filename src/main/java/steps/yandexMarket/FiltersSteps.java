package steps.yandexMarket;

import pages.yandexMarket.FiltersPage;
import ru.yandex.qatools.allure.annotations.Step;

public class FiltersSteps {

    @Step("задан параметр поиска от {0}")
    public void setMinPrice(String initialPrice) {
        new FiltersPage().setMinPrice(initialPrice);
    }

    @Step("задан параметр поиска до {0}")
    public void setMaxPrice(String initialPrice) {
        new FiltersPage().setMaxPrice(initialPrice);
    }

    @Step("выбран производитель {0}")
    public void setManufacturer(String name) {
        new FiltersPage().setManufacturer(name);
    }

    @Step("нажать кнопку Применить")
    public void pushAccept() {
        new FiltersPage().pushAccept();
    }

}
