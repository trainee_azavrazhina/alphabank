package steps.yandexMarket;

import pages.yandexMarket.SearchByNameResultPage;
import ru.yandex.qatools.allure.annotations.Step;

public class SearchResultByNameSteps {

    @Step("проверить значение имени товара")
    public void checkTitles() {
        new SearchByNameResultPage().checkTitles();
    }
}
