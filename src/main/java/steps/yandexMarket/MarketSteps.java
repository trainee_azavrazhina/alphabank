package steps.yandexMarket;

import pages.yandexMarket.MarketPage;
import ru.yandex.qatools.allure.annotations.Step;

public class MarketSteps {

    @Step("выбрана категория {0}")
    public void selectMenuItem(String itemName) {
        new MarketPage().selectMenuItem(itemName);
    }
}
