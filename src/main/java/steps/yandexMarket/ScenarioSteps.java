package steps.yandexMarket;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import steps.BaseSteps;

public class ScenarioSteps {

    BaseSteps baseSteps = new BaseSteps();
    MainPageSteps mainPageSteps = new MainPageSteps();
    MarketSteps marketSteps = new MarketSteps();
    ElectronicsSteps electronicsSteps = new ElectronicsSteps();
    ProductSteps productSteps = new ProductSteps();
    FiltersSteps filtersSteps = new FiltersSteps();
    FilterResultSteps filterResultSteps = new FilterResultSteps();
    SearchResultByNameSteps searchResultByNameSteps = new SearchResultByNameSteps();

    @Given("^открыта страница \"(.+)\"$")
    public void getPage(String url) {
        baseSteps.getPage(url);
    }

    @When("^выбран пункт меню (.+)$")
    public void selectMenuItem(String itemName) {
        mainPageSteps.selectMenuItem(itemName);
    }

    @When("^выбрана категория (.+)$")
    public void selectSection(String itemName) {
        marketSteps.selectMenuItem(itemName);
    }

    @When("^выбран раздел (.+)$")
    public void selectProductGroup(String itemName) {
        electronicsSteps.selectMenuItem(itemName);
    }

    @Then("зайти в расширенный поиск")
    public void goToAdvancedSearch() {
        productSteps.selectMenuItem();
    }

    @When("^задан параметр поиска от (.+)$")
    public void setMinPrice(String initialPrice) {
        filtersSteps.setMinPrice(initialPrice);
    }

    @When("^задан параметр поиска до (.+)$")
    public void setMaxPrice(String initialPrice) {
        filtersSteps.setMaxPrice(initialPrice);
    }

    @When("^выбран производитель (.+)$")
    public void setManufacturer(String name) {
        filtersSteps.setManufacturer(name);
    }

    @Then("нажать кнопку Применить")
    public void pushAccept() {
        filtersSteps.pushAccept();
    }

    @When("сохранено значение первого товара")
    public void searchingModel() {
        filterResultSteps.searchingModel();
    }

    @Then("проверить значение имени товара")
    public void checkTitles() {
        searchResultByNameSteps.checkTitles();
    }

}
