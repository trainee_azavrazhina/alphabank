package steps.alfabank;

import pages.alfabank.SearchResultPage;
import ru.yandex.qatools.allure.annotations.Step;

public class SearchResultSteps {

    @Step("открыть первый результат")
    public void openFirstResult() {
        new SearchResultPage().openFirstResult();
    }
}
