package steps.alfabank;

import pages.alfabank.SearchPage;
import ru.yandex.qatools.allure.annotations.Step;

public class SearchSteps {

    @Step("поле поиска заполнено значением {0}")
    public void fillSearchField(String value) {
        new SearchPage().fillSearchField(value);
    }

}
