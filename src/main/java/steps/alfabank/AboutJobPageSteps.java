package steps.alfabank;

import pages.alfabank.AboutJobPage;
import pages.alfabank.SearchResultPage;
import ru.yandex.qatools.allure.annotations.Step;

public class AboutJobPageSteps {

    @Step("выполнить печать информации в файл")
    public void printInfoToFile() {
        new AboutJobPage().printInfoToFile();
    }
}
