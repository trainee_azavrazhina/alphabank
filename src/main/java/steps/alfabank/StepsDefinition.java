package steps.alfabank;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import steps.BaseSteps;


public class StepsDefinition {

    BaseSteps baseSteps = new BaseSteps();
    SearchSteps searchSteps = new SearchSteps();
    SearchResultSteps searchResultSteps = new SearchResultSteps();
    MainPageSteps mainPageSteps = new MainPageSteps();
    JobPageSteps jobPageSteps = new JobPageSteps();
    AboutJobPageSteps aboutJobPageSteps = new AboutJobPageSteps();


    @Given("^загружена страница \"(.+)\"$")
    public void getPage(String url) {
        baseSteps.getPage(url);
    }

    @When("^поле поиска заполнено значением \"(.+)\"$")
    public void fillSearchField(String value) {
        searchSteps.fillSearchField(value);
    }

    @Then("^открыть первый результат$")
    public void openFirstResult() {
        searchResultSteps.openFirstResult();
    }

    @When("^выбрана вкладка из нижнего меню \"(.+)\"$")
    public void chooseOptionFromBottomMenu(String option) {
        mainPageSteps.chooseOptionFromBottomMenu(option);
    }

    @When("^сделан переход на вкладку \"(.+)\"$")
    public void chooseSection(String section) {
        jobPageSteps.chooseSection(section);
    }

    @Then("^выполнить печать информации в файл$")
    public void printInfoToFile() {
        aboutJobPageSteps.printInfoToFile();
    }


}
