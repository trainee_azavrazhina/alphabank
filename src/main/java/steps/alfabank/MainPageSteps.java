package steps.alfabank;

import pages.alfabank.MainPage;
import ru.yandex.qatools.allure.annotations.Step;

public class MainPageSteps {

    @Step("выбрана вкладка из нижнего меню {0}")
    public void chooseOptionFromBottomMenu(String option) {
        new MainPage().chooseOptionFromBottomMenu(option);
    }
}
