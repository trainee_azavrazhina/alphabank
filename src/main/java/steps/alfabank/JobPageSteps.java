package steps.alfabank;

import pages.alfabank.JobPage;
import ru.yandex.qatools.allure.annotations.Step;

public class JobPageSteps {

    @Step("сделан переход на вкладку {0}")
    public void chooseSection(String section) {
        new JobPage().chooseSection(section);
    }
}
